      
$(function() {
//script for tooltip
$("[data-toggle='tooltip']").tooltip();
//script for popover
$("[data-toggle='popover']").popover();
//script for carousel
$('.carousel').carousel({
    interval: 2000
});
//modal
$('#contratarModal').on('show.bs.modal',function(e){
    console.log('1-activando modal...');
    $('.btn-contratar').prop('disabled',true);
    $('.btn-contratar').removeClass('btn-primary');
    $('.btn-contratar').addClass('bt-secondary');
});
$('#contratarModal').on('shown.bs.modal',function(e){
    console.log('2-modal activo...');
});
$('#contratarModal').on('hide.bs.modal',function(e){
    console.log('3-ocultando modal...');
    $('.btn-contratar').prop('disabled',false);
    $('.btn-contratar').removeClass('btn-secondary');
    $('.btn-contratar').addClass('btn-primary');
});
$('#contratarModal').on('hidden.bs.modal',function(e){
    console.log('4-modal oculto...');
});
});    
